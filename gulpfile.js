const elixir = require('laravel-elixir');

// elixir.config.sourcemaps = app.env.NODE_ENV != 'production';
process.env.DISABLE_NOTIFIER = true;

elixir((mix) => {
	mix.webpack('../../../src/app.js', '/plugin/YikYakHack.js');
});
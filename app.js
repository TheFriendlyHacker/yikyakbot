(function() {
	var self = this;
	this.csrfToken = null;
	this.accessToken = null;
	this.lat = 34.1426163;
	this.long= -84.55844859999999;

	 this.Init = function(callback)
	{
		_fetchCsrfToken(function(csrfToken) {
			callback(csrfToken, _fetchAccessToken());
		});
	};

	Init(function(csrfToken, accessToken) {
		this.csrfToken = csrfToken;
		this.accessToken = accessToken;

		setupAjax(csrfToken, accessToken);

		makePost("This needs to be done better", false);
	});


})(window);



function setupAjax(csrfToken, accessToken)
{
	$.ajaxSetup({
		headers: {
			'x-csrf-token': csrfToken,
			'x-access-token': accessToken
		}
	});
}

function makePost(message, handle)
{
	$.post({
		url: 'https://www.yikyak.com/api/v2/messages?userLat=34.142559&userLong=-84.5584224&lat=34.142559&long=-84.5584224&myHerd=0',
		data: JSON.stringify({"message": message, "handle": handle}),
		contentType: 'application/json',
	})
	.success(function(data) {
		console.log(data);
	});
}

function _fetchCsrfToken(callback)
{
	$.get('https://www.yikyak.com/nearby/new').success(function(html) {
		csrfToken = html.split('"csrfToken":')[1].replace('"', '').substring(0, 36);
		callback(csrfToken);
	});
}

function _fetchAccessToken()
{
	return _getCookie('yid');
}

function _getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length,c.length);
    }
  }
  return "";
}
/*
 * Copyright 2012-2016 the original author or authors
 * @license MIT, see LICENSE.txt for details
 *
 * @author Jeremy Grelle
 * @author Scott Andrews
 */

import config from './../config';
import API from './../API';
import Store from './../Store';

const interceptor = require('rest/interceptor');
const delay = require('rest/util/delay');

/**
 * Retries a rejected request using an exponential backoff.
 *
 * Defaults to an initial interval of 100ms, a multiplier of 2, and no max interval.
 *
 * @param {Client} [client] client to wrap
 * @param {number} [config.intial=100] initial interval in ms
 * @param {number} [config.multiplier=2] interval multiplier
 * @param {number} [config.max] max interval in ms
 *
 * @returns {Client}
 */
module.exports = interceptor({

	request: function (request, config) {
		let headers = request.headers || (request.headers = {});
		headers['x-access-token'] = Store.get('accessToken');
		headers['x-csrf-token'] = Store.get('csrfToken');
  	return request;
	},

	error: function (response, clientConfig, meta) {

		if(response.status.code === 401)
		{
			console.log("ACCESS TOKEN");
			API.refreshAccessToken().then((token) => {
				Store.dispatch('setAccessToken', token);

				let request = $.extend(true, {}, response.request);

				request.retry = config.intervalBetweenActions;

				return delay(request.retry, request).then(function (request) {

					if(request.hasOwnPropterty('entity')) {
						request.entity = $.parseJSON(request.entity);
					}

					if (request.canceled) {
						// cancel here in case client doesn't check canceled flag
						return Promise.reject({ request: request, error: 'precanceled' });
					}

					request.retry = config.intervalBetweenActions;
					return meta.client(request);
				});
			});
		}

	}
});
import interceptor from 'rest/interceptor';
const delay = require('rest/util/delay');
import Store from './../Store';

/**
 * Attach the access token to each request. If the access token is rejected,
 * request a new one and re-send the rejected request.
 */
export default interceptor({
	init: function (config) {
		config.initial = config.initial || 5000;
		config.multiplier = config.multiplier || 2;
		config.max = config.max || Infinity;
		return config;
	},

	request: function (request, config) {
		let headers = request.headers || (request.headers = {});
		headers['x-access-token'] = Store.get('accessToken');
		headers['x-csrf-token'] = Store.get('csrfToken');
  	return request;
	},

	/*error: (response, config, meta) => {
		let request = $.extend(true, {}, response.request);

		// if(response.status.code === 429)
		// {
			return delay(request.retry, request).then(function (request) {

				if(request.entity)
					request.entity = $.parseJSON(request.entity);

				if (request.canceled) {
					// cancel here in case client doesn't check canceled flag
					return Promise.reject({ request: request, error: 'precanceled' });
				}

				request.retry = Math.min(request.retry * config.multiplier, config.max);
				return meta.client(request);
			});
		// }


	}*/
});

const isAuthenticationError = (response) => {
	return response.status.code === 401;
};

const isRateLimitError = (response) => {
	return response.status.code === 429;
};

const refreshToken = (client) => {
	return new Promise((resolve, reject) => {
		client({path: 'auth/token/refresh'}).then((response) => {
			console.log(response);
			Store.dispatch('setAccessToken', response.entity);
			resolve(response.entity);
		});
	});
};

const prepareEntity = (request) => {
	if(request.entity)
	{
		request.entity = $.parseJSON(request.entity);
	}
	return request;
};

const handleCanceledRequest = (request) => {
	if (request.canceled) {
		return Promise.reject({ request: request, error: 'precanceled' });
	}
};
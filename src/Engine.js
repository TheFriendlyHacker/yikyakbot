import config from './config';

import API from './API';
import Store from './Store';

import Message from './models/Message';
import Comment from './models/Comment';

class Engine
{
	constructor()
	{

	}

	static initEngine()
	{
		this.start = performance.now();
		return new Promise((resolve, reject) => {
			var promises = [
				API.fetchAccessToken(),
				API.fetchCsrfToken(),
				API.fetchPhrases(),
				API.getCurrentLocation()
			];

			Promise.all(promises).then((values) => {
				Store.dispatch('setAccessToken', values[0]);
				Store.dispatch('setCsrfToken', values[1]);
				Store.dispatch('setPhrases', values[2]);
				Store.dispatch('setLocation', {lat: values[3].latitude, long: values[3].longitude});
				API.fetchUserData().then((response) => {
					/*console.log(`Hello ${Store.get('yakker.nickname')}`);
					console.log(`Current Yakarma: ${Store.get('yakker.yakarma')}`);
					console.log(`YikYakHack will generate ${Store.get('desiredYakarma')} Yakarma for you!`);
					console.log(`Approx. completion time: ${Store.get('desiredYakarma') / 2} seconds`);
					console.log(`BEGINNING GENERATION`);*/
					resolve(true);
				});
				resolve(true);
			}, (err) => {
			    console.log(err);
			    reject();
			});
		});
	}

	static finishJob()
	{
		let executionTime = (performance.now() - this.start) / 1000;
		console.log(`DONE! Generated ${Store.get('generatedYakarma')} Yakarma!`);
		console.log(`You now have ${Store.get('yakker.yakarma') + Store.get('desiredYakarma')} Yakarma.`);
		console.log(`Task completed in ${executionTime.toFixed(2)} seconds.`);
	}

	requestDesiredYakarma()
	{
		let desiredYakarma = null;
		// while(isNaN(desiredYakarma) || desiredYakarma === null)
		// {
		// 	desiredYakarma = prompt("How much Yakarma would you like to generate?");
		// }
		API.refreshAccessToken();
		return Store.dispatch('setDesiredYakarma', 3000);
	}

	getUserLocation()
	{
		API.getCurrentLocation().then((coords) => {
			console.log(coords.latitude);
			console.log(coords.longitude);
		})
	}

	generateYakarma()
	{
		let self = this;
		return new Promise((resolve, reject) => {
			API.postMessage(new Message(Store.getter('randomMessage'))).then((message) => {

				Store.dispatch('addYakarma', config.YikYak.Yakarma.postMessage);
				Store.dispatch('addMessage', message);

				API.upvoteMessage(message).then((message) => {

					Store.dispatch('addYakarma', (config.YikYak.Yakarma.giveUpvote + config.YikYak.Yakarma.receiveUpvote));

					function makeComment(message)
					{
						let t1 = performance.now();

						setTimeout(() => {

							self.commentOnMessage(message).then((comment) => {

								setTimeout(() => {
									self.upvoteComment(comment).then((comment) => {

										if(Store.get('generatedYakarma') < Store.get('desiredYakarma'))
										{
											let t2 = performance.now();
											let e1 = ((t2 - t1) / 1000).toFixed(1);
											let delay = (2.4 - e1) * 1000;
											console.log('');
											console.log(`Yakarma Generated: ${Store.get('generatedYakarma')}`);
											console.log(`Timestamp: ${moment.utc().local().format('h:mm:ss')}`)
											console.log(`Action Time: ${e1}`);
											console.log(`Time Elapsed: ${((t1 + t2)/1000).toFixed(2)}`);

											setTimeout(() => {
												makeComment(message);
											}, config.intervalBetweenActions);
										}
										else
										{
											setTimeout(() => {
												API.deleteMessage(message).then((message) => {
													Store.dispatch('addYakarma', config.YikYak.Yakarma.deleteMessage);
													resolve(true);
												});
											}, config.intervalBetweenActions);
										}
									});
								}, config.intervalBetweenActions);
							},
							(response) => {
								console.log("pause");
							});
						}, config.intervalBetweenActions);
					}
					makeComment(message);
				});
			});
		});
	}

	upvoteEverything()
	{
		let self = this;
		return new Promise((resolve, reject) => {
			let t1 = performance.now();

			// Retrieve the local feed
			console.log("Rertieving local feed...");
			console.log("");
			API.getLocalFeed().then((messages) => {
				_.forEach(messages, (message) => {
					Store.dispatch('addMessage', new Message(message));
				});

				let totalMessageCount = Store.get('messages').length;
				let upvotableMessages = Store.getter('upvotableMessages');
				let upvotableMessageCount = upvotableMessages.length;


				let numOfMessagesUpvoted = 0;

				console.log(`Retrieved ${Store.get('messages').length} messages.`);

				let promises = [];

				/**
				 * Iterate through the messages, upvote each one, and upvote the comments.
				 */
				if(!_.isEmpty(upvotableMessages))
				{
					(function upvoteMessages (i) {

						promises.push(new Promise((resolve, reject) => {
					   	let currentMessage = upvotableMessages[i];
				   		setTimeout(function () {
				   			if(!currentMessage.liked)
				   			{
						      API.upvoteMessage(currentMessage).then((message) => {
						      	numOfMessagesUpvoted++;
						      	console.log(`Message '${message.messageID}' upvoted (${i+1}/${Store.get('messages').length})`);
						      	resolve(message);
						      }, (error) => {
						      	console.log(error);
						      	reject(message);
						      });
						    }
						    i--;
						    if(i > 0) upvoteMessages(i);      //  decrement i and call myLoop again if i > 0
				      }, 3000);
						}));

					})(upvotableMessageCount - 1);
				}

				Promise.all(promises).then((response) => {
					let t2 = performance.now();

					console.log('%cTASK COMPLETE!', `color: ${config.style.primaryColor}; font-weight: bold`);
					console.log(`Total Messages: ${totalMessageCount}`);
					console.log(`Messages Upvoted: ${upvotableMessageCount}`);
					console.log(`Messages Not Upvoted: ${(totalMessageCount - upvotableMessageCount)}`);
					console.log(`Completion Time: ${((t2 - t1) / 1000).toFixed(1)}`);

					resolve({
						total: totalMessageCount,
						upvoted: upvotableMessageCount,
						notUpvoted: (totalMessageCount - upvotableMessageCount),
						completionTime: ((t2 - t1) / 1000).toFixed(1)
					});
				});



			}, (error) => {
				reject(error);
			});
		});
	}

	commentOnMessage(message)
	{
		return new Promise((resolve, reject) => {
			API.postComment(new Comment(message.id, Store.getter('randomComment'))).then((comment) => {
				Store.dispatch('addYakarma', config.YikYak.Yakarma.giveComment);
				Store.dispatch('addYakarma', config.YikYak.Yakarma.receiveComment);
				Store.dispatch('addComment', comment);
				resolve(comment);
			});
		});
	}

	upvoteComment(comment)
	{
		return new Promise((resolve, reject) => {
			API.upvoteComment(comment).then((comment) => {
				Store.dispatch('addYakarma', (config.YikYak.Yakarma.giveUpvote + config.YikYak.Yakarma.receiveUpvote));
				resolve(comment);
			});
		});
	}

	upvoteMessage(message)
	{
		return new Promise((resolve, reject) => {
			API.upvoteMessage(message).then((message) => {
				Store.dispatch('addYakarma', (config.YikYak.Yakarma.giveUpvote + config.YikYak.Yakarma.receiveUpvote));
				resolve(message);
			});
		});
	}

	deleteMessage(message)
	{
		return new Promise((resolve, reject) => {
			API.deleteMessage(message).then((message) => {
				Store.dispatch('addYakarma', config.YikYak.Yakarma.deleteMessage);
				resolve(message);
			});
		});
	}

	static set csrfToken(value)
	{
		this._csrfToken = value;
	}

	static get csrfToken()
	{
		return this._csrfToken;
	}

	static set accessToken(value)
	{
		this._accessToken = value;
	}

	static get accessToken()
	{
		return this._accessToken;
	}

}

export default Engine;
/**
 * Application State Management
 * Data which needs to be accessed throughout the application is stored here.
 */

import Message from './models/Message';
import Comment from './models/Comment';

const state = {
	accessToken: null,
	csrfToken: null,
	location: {
		latitude: null,
		longitude: null
	},
	desiredYakarma: 0,
	generatedYakarma: 0,
	messages: [],
	comments: [],
	yakker: {
		nickname: null,
		userID: null,
		personaID: null,
		yakarma: null
	},
	phrases: {
		messages: [],
		comments: []
	}
};

const actions = {

	setUserData: (data) => {
		state.yakker = data;
	},

	setPhrases: (data) => {
		state.phrases = data;
	},

	updateUserYakarma: (yakarma) => {
		state.yakker.yakarma = yakarma;
	},

	setAccessToken: (token) => {
		state.accessToken = token;
	},

	setCsrfToken: (token) => {
		state.csrfToken = token;
	},

	setDesiredYakarma: (amount) => {
		state.desiredYakarma = amount;
	},

	addMessage: (message) => {
		state.messages.push(message);
	},

	addComment: (comment) => {
		state.comments.push(comment);
	},

	addYakarma: (amount) => {
		state.generatedYakarma += amount;
	},

	resetGeneratedYakarma: () => {
		state.generatedYakarma = 0;
	},

	setLocation: ({lat, long}) => {
		state.location.latitude = lat;
		state.location.longitude = long;
	}

};

const getters = {

	randomMessage: () => {
		return _.sample(state.phrases.messages).substring(0, 200);
	},

	randomComment: () => {
		return _.sample(state.phrases.comments).substring(0, 200);
	},

	upvotableMessages: () => {
		return _.filter(state.messages, (message) => !message.liked);
	}

};


const Store = {
	dispatch: (action, payload) => {
		return actions[action](payload);
	},
	/**
	 * Retrieve the value of an item from state
	 * @param  {string} pointer
	 * @return {mixed}
	 */
	get: (pointer) => {
		return _.get(state, pointer, undefined);
	},

	/**
	 * Retrieve data from state using a getter.
	 * @param  {string} getter the name of the getter function
	 * @param  {mixed} args
	 * @return {mixed}
	 */
	getter: (getter, args) => {
		return getters[getter](args);
	}
};

export default Store;
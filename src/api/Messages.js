import Message from './../models/Message';
import Comment from './../models/Comment';
import Store from './../Store';

const baseUri = 'v2/messages{?userLat}{&userLong}{&lat}{&long}{&myHerd}';

export default {

	/**
	 * Post the given message to YikYak.
	 * @param  {string} message
	 * @param {bool} showHandle
	 * @return {Promise} resolve: {Message} reject: {mixed}
	 */
	postMessage: (message, showHandle = false) => {
		return new Promise((resolve, reject) => {
			Client({path: baseUri,
				entity: {
					message: message,
					handle: showHandle
				},
				params: {
					myHerd: message.myHerd,
					userLat: Store.get('location.latitude'),
					userLong: Store.get('location.longitude'),
					lat: Store.get('location.latitude'),
					long: Store.get('location.longitude'),
				}
			}).then((response) => {
				let message = new Message(response.entity);
				resolve(message);
			}, (response) => {
				reject(response);
			});
		});
	},

	getMessage: (id) => {

	},

	/**
	 * Upvote the given message.
	 * @param  {Message} message
	 * @return {Promise} resolve: {Message} reject: {mixed}
	 */
	upvoteMessage: (message, myHerd = 0) => {
		return new Promise((resolve, reject) => {
			if(!message.liked)
			{
				Client({ path: baseUri,
					method: 'PUT',
					// entity: {},
					params: {
						messageID: message.messageID,
						userLat: Store.get('location.latitude'),
						userLong: Store.get('location.longitude'),
						lat: Store.get('location.latitude'),
						long: Store.get('location.longitude'),
						myHerd: myHerd
					}
				})
				.then((response) => {
					message.numberOfLikes++;
					message.liked = 1;
					resolve(message);
				});
			}
			else
			{
				resolve(message);
			}
		});
	},

	/**
	 * Upvote the given message.
	 * @param  {Message} message
	 * @return {Promise} resolve: {Message} reject: {mixed}
	 */
	downvoteMessage: (message, myHerd = 0) => {
		return new Promise((resolve, reject) => {
			if(!message.liked)
			{
				Client({ path: baseUri,
					method: 'PUT',
					// entity: {},
					params: {
						messageID: message.messageID,
						userLat: Store.get('location.latitude'),
						userLong: Store.get('location.longitude'),
						lat: Store.get('location.latitude'),
						long: Store.get('location.longitude'),
						myHerd: myHerd
					}
				})
				.then((response) => {
					message.numberOfLikes++;
					message.liked = 1;
					resolve(message);
				});
			}
			else
			{
				resolve(message);
			}
		});
	},

	deleteMessage: (id) => {

	}

};
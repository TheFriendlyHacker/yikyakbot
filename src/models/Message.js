class Message
{
	/**
	 * Class constructor.
	 * @param {obj} messageObj plain object from the server.
	 */
	constructor(messageObj)
	{
		$.extend(true, this, messageObj);
		this.comments = null;
	}

	/**
	 * Upvote the current message.
	 * @param  {bool} myHerd
	 * @return {Promise} resolve: {Message} reject: {mixed}
	 */
	upvote(myHerd = 0)
	{
		return new Promise((resolve, reject) => {
			let self = this;
			if(this.liked != 1)
			{
				Client({ path: 'v2/messages{/messageID}/upvote{?userLat}{&userLong}{&lat}{&long}{&myHerd}',
					method: 'PUT',
					params: {
						messageID: self.messageID,
						userLat: Store.get('location.latitude'),
						userLong: Store.get('location.longitude'),
						lat: Store.get('location.latitude'),
						long: Store.get('location.longitude'),
						myHerd: myHerd
					}
				})
				.then((response) => {
					self.numberOfLikes++;
					self.liked++;
					resolve(self);
				});
			}
			else
			{
				resolve(self);
			}
		}, (error) => {
			reject(error);
		});
	}

	/**
	 * Downvote the current message.
	 * @param  {bool} myHerd
	 * @return {Promise} resolve: {Message} reject: {mixed}
	 */
	downvote(myHerd = 0)
	{
		return new Promise((resolve, reject) => {
			let self = this;
			if(this.liked != -1)
			{
				Client({ path: 'v2/messages{/messageID}/downvote{?userLat}{&userLong}{&lat}{&long}{&myHerd}',
					method: 'PUT',
					params: {
						messageID: self.messageID,
						userLat: Store.get('location.latitude'),
						userLong: Store.get('location.longitude'),
						lat: Store.get('location.latitude'),
						long: Store.get('location.longitude'),
						myHerd: myHerd
					}
				})
				.then((response) => {
					self.numberOfLikes--;
					self.liked--;
					resolve(self);
				});
			}
			else
			{
				resolve(self);
			}
		}, (error) => {
			reject(error);
		});
	}

	/**
	 * Delete the message
	 * @return {Promise} resolve: {Message} reject: {mixed}
	 */
	delete()
	{
		//
	}

	/**
	 * Fetch the comments for the message.
	 * @param  {bool} myHerd
	 * @return {Promise} resolve: {Message} reject: {mixed}
	 */
	fetchComments()
	{
		return new Promise((resolve, reject) => {
			let self = this;
			Client({ path: 'v2/messages{/messageID}/comments{?userLat}{&userLong}',
				method: 'GET',
				params: {
					messageID: self.messageID,
					userLat: Store.get('location.latitude'),
					userLong: Store.get('location.longitude'),
				}
			})
			.then((response) => {
				let comments = response.entity;
				self.comments = comments;
				resolve(comments);
			});
		}, (error) => {
			reject(error);
		});
	}

	/**
	 * Determine if the message has already been upvoted.
	 * @return {bool}
	 */
	isUpvoted()
	{
		return this.liked === 1;
	}

	/**
	 * Determine if the message has already been downvoted.
	 * @return {bool}
	 */
	isDownvoted()
	{
		return this.liked === -1;
	}

	/**
	 * Determine if the message has neither been upvoted or downvoted.
	 * @return {bool}
	 */
	isNeutral()
	{
		return this.liked === 0;
	}

	/**
	 * Refresh the comment by retrieving it from the server and updating it here.
	 * @return {Promise} resolve: {Message} reject: {mixed}
	 */
	refresh()
	{

	}



}
export default Message;
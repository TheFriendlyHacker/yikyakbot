import Store from './Store';

export default {
	appBaseUrl: 'https://www.yikyak.com/nearby',
	apiBaseUrl: 'https://www.yikyak.com/api',
	defaultRequest: {
		headers: {
      // 'X-Requested-With': 'rest.js',
      'Content-Type': 'application/json',
      'Cache-Control': 'max-age=0'
    }
  },
  defaultParams: {
    // userLat: Store.get('location.latitude'),
    // userLong: Store.get('location.longitude'),
    // lat: Store.get('location.latitude'),
    // long: Store.get('location.longitude'),
    myHerd: 0,
    formFactor: 'desktop',
    os: 'Mac OS 10.12.1'
  },

  // The maximum amount of Yakarma that can be generated per session
  maxRequestedYakarma: 5000,
  intervalBetweenActions: 2000, //the number of milliseconds between
  rateLimitWaitInterval: 5000, //if the ratelimit is exceeded, wait this long before continuing

  YikYak: {
  	Yakarma: {
  		postMessage: 2,
      deleteMessage: 0,
      giveComment: 2,
      receiveComment: 0,
      deleteComment: 0,
  		giveUpvote: 1,
  		receiveUpvote: 2,
  		giveDownvote: 0,
  		receiveDownvote: -2,
  	},
    location: {
      latitude: 12.00000000,
      longitude: 12.00000000
    },
  	rateLimit: {
  		amount: 15, //number of actions you can complete via the rate limit
  		interval: 30, //the duration of the rate limit, in seconds
  	},
  },

  style: {
    primaryColor: "#0dd5b2"
  }
};
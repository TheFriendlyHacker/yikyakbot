/**
 * Jquery 2.2.4 - because everyone loves Jquery
 */
window.$ = window.jQuery = window.Jquery = require('jquery');

/**
 * Moment.js - create nicely formatted timestamps
 */
window.moment = require('moment');

/**
 * Lodash.js - a bunch of nice helper methods for JavaScript
 * https://lodash.com/
 */
window._ = require('lodash');

/**
 * Bring in the configuration. This will be used throughout the app.
 */
window.config = require('./config');
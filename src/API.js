import config from './config';
/**
 * Promise.js
 * https://www.promisejs.org/
 */
import Promise from 'promise';
/**
 * Load our Http client, powered by Rest.js. This is what we will
 * use to communicate with YikYak's API.
 */
import Client from './Client';
/**
 * Bring in our state manager
 */
import Store from './Store';

/**
 * Some models...
 */
import Message from './models/Message';
import Comment from './models/Comment';

/**
 * API methods for interacting with YikYak.
 */
const API = {

	fetchCsrfToken: () => {
		return new Promise((resolve, reject) => {
			Client({path: `${config.appBaseUrl}/nearby/new`}).then((response) => {
				let csrfToken = response.entity.split('"csrfToken":')[1].replace('"', '').substring(0, 36);
				resolve(csrfToken);
			}, (response) => {
				console.log(response);
			});
		});
	},

	fetchAccessToken: () => {
		return new Promise((resolve, reject) => {
			resolve(getCookie('yid'));
		}, (response) => {
			reject(response);
		});
	},

	refreshAccessToken: () => {
		return new Promise((resolve, reject) => {
			Client({path: `${config.apiBaseUrl}/auth/token/refresh`}).then((token) => {
				resolve(token.entity);
			});
		});
	},

	/**
	 * Fetch a list of message + comment phrases to use when posting messages/comments.
	 * @return {Promise} resolve: {Object} reject: {mixed}
	 */
	fetchPhrases: () => {
		return new Promise((resolve, reject) => {
			let phrases = require('./phrases').default;
			resolve(phrases);
		});
	},

	/**
	 * Fetch data about the current user.
	 * @return {Promise} resolve: {Object} reject: {mixed}
	 */
	fetchUserData: () => {
		return new Promise((resolve, reject) => {
			Client({
				path: 'v2/yakker/init{?userLat}{&userLong}{&myHerd}{&formFactor}{&os}',
				params: {
					userLat: Store.get('location.latitude'),
					userLong: Store.get('location.longitude'),
					lat: Store.get('location.latitude'),
					long: Store.get('location.longitude'),
				}
			})
			.then((response) => {
				Store.dispatch('setUserData', response.entity.yakker);
				resolve(true);
			});
		});
	},

	getCurrentLocation: () => {
		return new Promise((resolve, reject) => {
			navigator.geolocation.getCurrentPosition((pos) => {
				resolve({
					latitude: pos.coords.latitude,
					longitude: pos.coords.longitude
				});
			}, (error) => {
				switch(error.code) {
		      case error.PERMISSION_DENIED:
	          reject("User denied the request for Geolocation.");
	          break;
		      case error.POSITION_UNAVAILABLE:
	          reject("Location information is unavailable.");
	          break;
		      case error.TIMEOUT:
		        reject("The request to get user location timed out.");
		        break;
		      case error.UNKNOWN_ERROR:
	          reject("An unknown error occurred.");
	          break;
		    }
			});
		});
	},

	getLocalFeed: () => {
		return new Promise((resolve, reject) => {
			Client({
				path: 'v2/messages{?userLat}{&userLong}{&lat}{&long}{&feedType}',
				params: {
					userLat: Store.get('location.latitude'),
					userLong: Store.get('location.longitude'),
					lat: Store.get('location.latitude'),
					long: Store.get('location.longitude'),
					feedType: 'new'
				}
			})
			.then((response) => {
				resolve(response.entity);
			}, (response) => {
				reject(response);
			});
		});
	},

	/**
	 * Post the given message to YikYak.
	 * @param  {string} message
	 * @param {bool} showHandle
	 * @return {Promise} resolve: {Message} reject: {mixed}
	 */
	postMessage: (message, showHandle = false) => {
		return new Promise((resolve, reject) => {
			Client({path: 'v2/messages{?userLat}{&userLong}{&lat}{&long}{&myHerd}',
				entity: {
					message: message,
					handle: showHandle
				},
				params: {
					myHerd: message.myHerd
				}
			}).then((response) => {
				let message = new Message(response.entity);
				resolve(message);
			}, (response) => {
				reject(response);
			});
		});
	},

	/**
	 * Post the given comment to YikYak.
	 * @param  {Comment} comment
	 * @return {Promise} resolve: {Comment} reject: {mixed}
	 */
	postComment: (comment) => {
		return new Promise((resolve, reject) => {
			Client({path: 'v2/messages{/messageId}/comments{?userLat}{&userLong}{&lat}{&long}{&myHerd}',
				entity: {
					comment: comment.body,
					handle: comment.showHandle
				},
				params: {
					myHerd: comment.myHerd,
					messageId: comment.messageId
				}
			}).then((response) => {
				comment.id = response.entity.commentID;
				resolve(comment);
			}, (response) => {
				reject(response);
			});
		});
	},

	/**
	 * Upvote the given comment.
	 * @param  {Comment} comment
	 * @return {Promise} resolve: {Comment} reject: {mixed}
	 */
	upvoteComment: (comment) => {
		return new Promise((resolve, reject) => {
			Client({ path: 'v2/messages{/messageId}/comments{/commentId}/upvote{?userLat}{&userLong}{&myHerd}',
				method: 'PUT',
				// entity: {},
				params: {
					messageId: comment.messageId,
					commentId: comment.id,
					myHerd: comment.myHerd
				}
			})
			.then((response) => {
				comment.upvotes++;
				resolve(comment);
			}, (response) => {
				reject(response);
			});
		});
	},

	/**
	 * Upvote the given message.
	 * @param  {Message} message
	 * @return {Promise} resolve: {Message} reject: {mixed}
	 */
	upvoteMessage: (message, myHerd = 0) => {
		return new Promise((resolve, reject) => {
			if(!message.liked)
			{
				Client({ path: 'v2/messages{/messageID}/upvote{?userLat}{&userLong}{&myHerd}',
					method: 'PUT',
					// entity: {},
					params: {
						messageID: message.messageID,
						userLat: Store.get('location.latitude'),
						userLong: Store.get('location.longitude'),
						lat: Store.get('location.latitude'),
						long: Store.get('location.longitude'),
						myHerd: myHerd
					}
				})
				.then((response) => {
					message.numberOfLikes++;
					message.liked = 1;
					resolve(message);
				});
			}
			else
			{
				resolve(message);
			}
		});
	},

	/**
	 * Delete the given message from YikYak.
	 * @param  {Message} message
	 * @return {Promise} resolve: {Message} reject: {mixed}
	 */
	deleteMessage: (message) => {
		return new Promise((resolve, reject) => {
			Client({
				path: 'v2/messages{/messageId}{?userLat}{&userLong}{&myHerd}',
				method: 'DELETE',
				params: {
					messageId: message.id,
					myHerd: message.myHerd
				}
			})
			.then((response) => {
				message.deleted = true;
				resolve(message);
			});
		});
	}

};

const getCookie = (cname) => {
	let name = cname + "=";
	let ca = document.cookie.split(';');
	for(let i = 0; i <ca.length; i++) {
	  let c = ca[i];
	  while (c.charAt(0)==' ') {
	    c = c.substring(1);
	  }
	  if (c.indexOf(name) === 0) {
	    return c.substring(name.length,c.length);
	  }
	}
	return "";
};

export default API;
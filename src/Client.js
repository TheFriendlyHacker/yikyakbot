/**
 * Rest.js - a nice RESTful AJAX client
 * https://github.com/cujojs/rest/tree/master/docs
 */
import config from './config';

const rest = require('rest');
const pathPrefix = require('rest/interceptor/pathPrefix');
const mime = require('rest/interceptor/mime');
const defaultRequest = require('rest/interceptor/defaultRequest');
const errorCode = require('rest/interceptor/errorCode');
const template = require('rest/interceptor/template');
const AccessTokenInterceptor = require('./interceptors/AccessToken');
const RateLimitInterceptor = require('./interceptors/RateLimit');
// const RetryInterceptor = require('rest/interceptor/retry');


const Client = rest.wrap(pathPrefix, { prefix: config.apiBaseUrl })
                    .wrap(mime)
                    .wrap(defaultRequest, config.defaultRequest)
                    .wrap(errorCode, { code: 400 })
                    .wrap(template, {params: config.defaultParams})
                    .wrap(AccessTokenInterceptor, { initial: config.rateLimitWaitInterval, max: config.rateLimitWaitInterval * 2 })
                    .wrap(RateLimitInterceptor, { initial: config.rateLimitWaitInterval, max: config.rateLimitWaitInterval * 2 })

export default Client;
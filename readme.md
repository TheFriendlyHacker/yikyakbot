# YikYak Voting Bot #

A very simple (and very incomplete) Yik Yak bot that browses through a given feed and upvotes all posts + comments on your behalf.

### What is this for? ###

* Fun
* That's about it

### Incomplete? ###

Unfortunately, this project is very incomplete, and I do not have any plans to finish it up in the near future. Below is a list of features and their "completeness":

* No GUI/interface; the bot shows you a pop-up, asking if you would like to launch it, and then it just goes.
* The bot will upvote all posts in the feed, but will not upvote comments.
* Once it has finished going through the feed, it simply terminates (aka, it does not refresh/check for new posts to upvote)
* My code is kinda messy and not super-well documented at this time.

### How do I get set up? ###

Right now, this is an unpackaged Chrome plugin. Check out the manifest.json file for a list of which files you should include. Google "how to install a Google Chrome dev plugin".

### Contribution guidelines ###

Do whatever you want.